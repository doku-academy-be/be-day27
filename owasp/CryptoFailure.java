public class CryptoFailure {
    //Contoh register user yang tidak mengenkripsi password
    public CreateUserResponse create(CreateUserRequest req) {
        User user = toEntity(req);
        Optional<User> userOptional = userRepository.findByEmail(user.getEmail());
        if(userOptional.isPresent()){
            throw new  ResponseStatusException(HttpStatus.CONFLICT,"User alredy exist");
        }
        User res  = userRepository.save(user);
        return toDto(res);
    }

    //Register user yang passwordnya terenkripsi

    //di class configuration
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    //kemudian pada class service dibagian register set password dengan password encoder
    @Autowired
    PasswordEncoder passwordEncoder;

    public CreateUserResponse create(CreateUserRequest req) {
        User user = toEntity(req);
        Optional<User> userOptional = userRepository.findByEmail(user.getEmail());
        if(userOptional.isPresent()){
            throw new  ResponseStatusException(HttpStatus.CONFLICT,"User alredy exist");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword())); // disini password yang didaftarkan user di encode menggunakan bcrypt
        User res  = userRepository.save(user);
        return toDto(res);
    }

}
