public class Misconfiguration {
    //Contoh misconfiguration
    //Ketika selesai mendevelop lupa mendisable service dan membiarkan port terbuka pada security config di bagian filternya
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
        return http
                .csrf(c -> c.disable())
                .authorizeRequests(a -> {
                            a.antMatchers(
                                    "/auth/**",
                                    "/account/**" // membiarkan port service account terbuka sehingga user tanpa melakukan proses auth bisa masuk
                            ).permitAll();
                            a.anyRequest().authenticated();
                        }
                )
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
                .sessionManagement(s -> s.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .httpBasic(Customizer.withDefaults())
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .build();
    }

}
